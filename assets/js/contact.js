class Contact {

  constructor(naam, email, Onderwerp, Bericht) {
    this.naam = naam.value;
    this.email = email.value;
    this.Onderwerp = Onderwerp.value;
    this.Bericht = Bericht.value;
  }

  isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
  }

  validate() {
    if (this.naam.length > 0 && (this.email.length > 0 && this.isEmail(this.email)) && this.Onderwerp.length > 0 && this.Bericht.length > 0) {
      return true
    }
    return false
  }
}

const getElement = (element) => document.getElementById(element);

const naam = getElement('naam')
const email = getElement('Email')
const Onderwerp = getElement('Onderwerp')
const Bericht = getElement('Bericht')

function validateForm() {
  const contact = new Contact(naam, email, Onderwerp, Bericht);
  const Message = getElement('Message')
  Message.style.display="block";
  if (contact.validate()) {
    Message.classList.add('success');
    Message.classList.remove('error');
    Message.innerHTML = "Success"
    return true
  }

  Message.classList.add('error');
  Message.classList.remove('success');
  Message.innerHTML = "All fields are required Or email is not valied"
  return false

}
